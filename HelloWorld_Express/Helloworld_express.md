## Exemple Hello world   Ouiiiiiiiiiiii
Il s’agit de l’application Express la plus simple que vous puissiez créer. Cette application ne contient qu’un seul fichier, c’est-à-dire tout l’inverse de ce que vous obtiendriez avec le générateur Express, qui crée l’échafaudage d’une application entière, avec des fichiers JavaScript, des modèles Pug et des sous-répertoires pour divers motifs.

Réaliser un HelloWorld avec l'aide de Express.js

Express:https://expressjs.com/en/api.html
